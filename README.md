# OpenML dataset: Household-monthly-electricity-bill

https://www.openml.org/d/43588

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Introduction
The idea behind this dataset is to see how the number of people and the home size affects the monthly electricity consumption in the household.
Column decription:



Column
Explanation




num_rooms
Number of room in the house


num_people
Number of people in the house


housearea
Area of the house


is_ac
Is AC present in the house?


is_tv
Is TV present in the house?


is_flat
Is house a flat?


avemonthlyincome
Average monthly income of the household


num_children
Number of children in the house


is_urban
Is the house present in an urban area


amount_paid
Amount paid as the monthly bill



Acknowledgements
This dataset was prepared as a mock up dataset for practice use

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43588) of an [OpenML dataset](https://www.openml.org/d/43588). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43588/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43588/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43588/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

